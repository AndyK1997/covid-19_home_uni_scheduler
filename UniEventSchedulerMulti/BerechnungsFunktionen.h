#pragma once

#include <time.h>
#include <Windows.h>
#include <string>
using namespace std;

//Automatische n�chste Termin Berechnung
tm newEventTime(tm tmp, int turnus) {
    int SizeOfMonth = 0;
    switch (tmp.tm_mon) {
    case 0: SizeOfMonth = 31; break;
    case 2: SizeOfMonth = 28; break;
    case 3: SizeOfMonth = 30; break;
    case 4: SizeOfMonth = 31; break;
    case 5: SizeOfMonth = 30; break;
    case 6: SizeOfMonth = 31; break;
    case 7: SizeOfMonth = 31; break;
    case 8: SizeOfMonth = 30; break;
    case 9: SizeOfMonth = 31; break;
    case 10: SizeOfMonth = 30; break;
    case 11: SizeOfMonth = 31; break;
    default:SizeOfMonth = 30; break;
    }
    if (tmp.tm_mday > SizeOfMonth) {
        tmp.tm_mon += 1;
        tmp.tm_mday = ((tmp.tm_mday + turnus) % SizeOfMonth) + 1;
    }
    else {
        tmp.tm_mday += turnus;
    }
    return tmp;
}

//F�hrt den Termin aus
void shellexec(string url) {
    wstring Url(url.begin(), url.end());
    LPCWSTR Website = Url.c_str();
    ShellExecute(0, L"open", L"firefox.exe", Website, 0, SW_SHOWDEFAULT);
}

//Berechnert die dauer bis zum n�chsten termin
long sleepCalc(tm Current, tm next) {
    long sleepTime = 10;

    if (next.tm_mday - Current.tm_mday != 0) {
        int stunden = 24 - Current.tm_hour;
        long TimePerDay = ((next.tm_mday - Current.tm_mday) - 1) * 24 * 60 * 60 * 1000;
        sleepTime = stunden * 60 * 60 * 1000 + TimePerDay + next.tm_hour * 60 * 60 * 1000;
    }
    else if (next.tm_mon - Current.tm_mon != 0) {
        int SizeOfMonth = 0;
        switch (Current.tm_mon) {
        case 0: SizeOfMonth = 31; break;
        case 2: SizeOfMonth = 28; break;
        case 3: SizeOfMonth = 30; break;
        case 4: SizeOfMonth = 31; break;
        case 5: SizeOfMonth = 30; break;
        case 6: SizeOfMonth = 31; break;
        case 7: SizeOfMonth = 31; break;
        case 8: SizeOfMonth = 30; break;
        case 9: SizeOfMonth = 31; break;
        case 10: SizeOfMonth = 30; break;
        case 11: SizeOfMonth = 31; break;
        default:SizeOfMonth = 30; break;
        }
        sleepTime = ((SizeOfMonth - Current.tm_mday) + next.tm_mday - 1) * 24 * 60 * 60 * 1000 + (24 - Current.tm_hour) * 60 * 60 * 1000 + (next.tm_hour * 60 * 60 * 1000 + next.tm_min * 60 * 1000);
    }
    else {
        sleepTime = (next.tm_hour * 60 * 60 * 1000 + next.tm_min * 60 * 1000) - (Current.tm_hour * 60 * 60 * 1000 + Current.tm_min * 60 * 1000);
    }

    return sleepTime - 10 * 60 * 1000;
}

//Erzeugt die aktuelle Zeit
tm currentTime() {
    time_t now;
    now = time(NULL);
    tm Time = *localtime(&now);

    return Time;
}


