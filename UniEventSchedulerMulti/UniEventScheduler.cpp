// UniEventScheduler.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//
#include <iostream>
#include <cstdlib>
#include <string>
#include "UniTermin.h"
#include <Windows.h>
#include <ctime>
#include <vector>
#include <thread>
#include <mutex>
#include "Data.h"
#include "BerechnungsFunktionen.h"
using namespace std;
mutex myMutex;
mutex killSignal;
enum Weekday { Sunnday, Monday, Tuesday, Wendnesday, Thursday, Friday, Saturday };
Data d;
bool progKill = false;
vector<UniTermin*> allEvents;


void Eventhandler() {
	tm Time;
	long sleeptime = 0;
	bool Kill = false;
	while (Kill==false) {
		Time = currentTime();

		if (Time.tm_wday == Sunnday || Time.tm_wday == Saturday) {
			sleeptime = 1000 * 60 * 12;
		}
		else {
			lock_guard<mutex> guard(myMutex);
			d.read();
			allEvents.clear();
			allEvents = d.getEvents();
			for (unsigned int x = 0; x < allEvents.size(); x++) {
				if (allEvents[x]->getTermin().tm_wday == Time.tm_wday && allEvents[x]->getTermin().tm_hour == Time.tm_hour && allEvents[x]->getTermin().tm_min == Time.tm_min && allEvents[x]->getTermin().tm_mon == Time.tm_mon+1 && allEvents[x]->getTermin().tm_mday == Time.tm_mday) {
					shellexec(allEvents[x]->getUrl());
					if (allEvents[x] == allEvents.back()) {
						sleeptime = sleepCalc(Time, allEvents[0]->getTermin());
					}
					else {
						sleeptime = sleepCalc(Time, allEvents[x + 1]->getTermin());
					}
					allEvents[x]->setTermin(newEventTime(Time, allEvents[x]->getTurnus()));
					d.setTmpEvents(allEvents);
					d.write();
					break;
				}
				else {
					sleeptime = 1000 * 60*10;
				}
			}
		}
		
		killSignal.lock();
			 Kill = progKill;
		killSignal.unlock();
	}
	exit(EXIT_SUCCESS);
}


void createEvent() {
	string Terminbeschreibung;
	string Turnus;
	string Datum;
	string Uhrzeit;
	string Wochentag;
	string URL;
	tm EventTime;
	
	cout << "Termin beschreibung: ";
	getline(cin, Terminbeschreibung);
	cout << "in wie viel Tagen wiederholen (0 für einmalig))";
	getline(cin, Turnus);
	cout << "Uhrzeit: (Format: hh:mm):";
	getline(cin, Uhrzeit);
	cout << "Datum(Format: dd.mm.yyyy) :";
	getline(cin, Datum);
	cout << "Wochentag (Sonntag=0): ";
	getline(cin,Wochentag);
	cout << "Webseite (URL):";
	getline(cin, URL);
	
	EventTime.tm_mday = atoi(Datum.substr(0, 2).c_str());
	EventTime.tm_mon = atoi(Datum.substr(3, 2).c_str());
	EventTime.tm_year = atoi(Datum.substr(6, 4).c_str());
	EventTime.tm_hour = atoi(Uhrzeit.substr(0,2).c_str());
	EventTime.tm_min = atoi(Uhrzeit.substr(3, 2).c_str());
	EventTime.tm_wday = atoi(Wochentag.c_str());

	myMutex.lock();
	allEvents.push_back(new UniTermin(Terminbeschreibung, atoi(Turnus.c_str()), EventTime, URL));
	d.setTmpEvents(allEvents);
	d.write();
	myMutex.unlock();
	system("CLS");
	exit(EXIT_SUCCESS);
}


int main(){
    thread *allThreads;
	allThreads = new thread[2];
	allThreads[0]= thread(Eventhandler);

	while (progKill==false)
	{
		cout << "Uni Eventscheduler" << endl;
		cout << "1 : Neuer Termin" << endl; 
		cout << "0 : programmEnde" << endl;
		string Eingabe;
		int e;
		getline(cin, Eingabe);
			e = atoi(Eingabe.c_str());
			switch (e) {
			case 1: allThreads[1] = thread(createEvent); allThreads[1].join();
				break;
			case 0: killSignal.lock(); progKill = true; killSignal.unlock();
				break;
			default: cerr << "Invalid option" << endl;
			}
		
	}
		
	

	allThreads[0].join();

	exit(EXIT_SUCCESS);
      
}

// Programm ausführen: STRG+F5 oder Menüeintrag "Debuggen" > "Starten ohne Debuggen starten"
// Programm debuggen: F5 oder "Debuggen" > Menü "Debuggen starten"

// Tipps für den Einstieg: 
//   1. Verwenden Sie das Projektmappen-Explorer-Fenster zum Hinzufügen/Verwalten von Dateien.
//   2. Verwenden Sie das Team Explorer-Fenster zum Herstellen einer Verbindung mit der Quellcodeverwaltung.
//   3. Verwenden Sie das Ausgabefenster, um die Buildausgabe und andere Nachrichten anzuzeigen.
//   4. Verwenden Sie das Fenster "Fehlerliste", um Fehler anzuzeigen.
//   5. Wechseln Sie zu "Projekt" > "Neues Element hinzufügen", um neue Codedateien zu erstellen, bzw. zu "Projekt" > "Vorhandenes Element hinzufügen", um dem Projekt vorhandene Codedateien hinzuzufügen.
//   6. Um dieses Projekt später erneut zu öffnen, wechseln Sie zu "Datei" > "Öffnen" > "Projekt", und wählen Sie die SLN-Datei aus.
