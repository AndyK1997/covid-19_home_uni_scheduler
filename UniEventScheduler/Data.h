#pragma once
#include "UniTermin.h"
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
using namespace std;
class Data {
private:
    vector<UniTermin*> tmpEvents;
    string sourcePath;
public:
    Data();
    void read();
    void write();
    vector<UniTermin*> getEvents() {
        return this->tmpEvents;
    }
    void setTmpEvents(vector<UniTermin*> tmp) {
        tmpEvents = tmp;
    }
};

