// UniEventScheduler.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//
#include <iostream>
#include <cstdlib>
#include <string>
#include "UniTermin.h"
#include <Windows.h>
#include <ctime>
#include <vector>
#include "Data.h"
#include <signal.h>
using namespace std;

//Globale Variablen 
UniTermin *NewEvent;
bool add = false;
bool addAgain = false;

//Signal Handler für strg+c
void mySignalHandler(int signum) {
    add = true;
    std::string VorlesungsName;
    int Turnus;
    tm Termin;
    std::string URL;
    cout << "Termin Name: "; cin >> VorlesungsName;
    cout << "in wie vielen tagen wiederholen 0=nie: "; cin >> Turnus;
    cout << "Datums Tag: "; cin >> Termin.tm_mday;
    cout << "Datums Monat: ";  cin >> Termin.tm_mon;
    cout << "Datums Jahr: "; cin >> Termin.tm_year;
    cout << "Wochentag (Sonntag=0): "; cin >> Termin.tm_wday;
    cout << "Uhrzeit Stunde: "; cin >> Termin.tm_hour;
    cout << "Uhrzeit Minute: "; cin >> Termin.tm_min;
    cout << "Ort (link): "; cin >> URL;

    if (Turnus != 0) {
        addAgain = true;
    }
    NewEvent = new UniTermin(VorlesungsName, Turnus, Termin, URL);
    
}

//Automatische nächste Termin Berechnung
tm newEventTime(tm tmp, int turnus) {
    int SizeOfMonth = 0;
    switch (tmp.tm_mon) {
    case 0: SizeOfMonth = 31; break;
    case 2: SizeOfMonth = 28; break;
    case 3: SizeOfMonth = 30; break;
    case 4: SizeOfMonth = 31; break;
    case 5: SizeOfMonth = 30; break;
    case 6: SizeOfMonth = 31; break;
    case 7: SizeOfMonth = 31; break;
    case 8: SizeOfMonth = 30; break;
    case 9: SizeOfMonth = 31; break;
    case 10: SizeOfMonth = 30; break;
    case 11: SizeOfMonth = 31; break;
    default:SizeOfMonth = 30; break;
    }
    if (tmp.tm_mday > SizeOfMonth) {
        tmp.tm_mon += 1;
        tmp.tm_mday = ((tmp.tm_mday + turnus) % SizeOfMonth) + 1;
    }
    else {
        tmp.tm_mday += turnus;
    }
    return tmp;
}
void shellexec(string url) {
    wstring Url(url.begin(), url.end());
    LPCWSTR Website = Url.c_str();
    ShellExecute(0, L"open", L"firefox.exe", Website, 0,SW_SHOWDEFAULT);
}


long sleepCalc(tm Current, tm next) {
    long sleepTime = 10;

    if (next.tm_mday - Current.tm_mday != 0) {
        int stunden = 24 - Current.tm_hour;
        long TimePerDay = ((next.tm_mday - Current.tm_mday)-1) * 24 * 60 * 60 * 1000;
        sleepTime = stunden * 60 * 60 * 1000 + TimePerDay + next.tm_hour * 60 * 60 * 1000;
    }
    else if (next.tm_mon-Current.tm_mon!=0){
        int SizeOfMonth = 0;
        switch (Current.tm_mon) {
        case 0: SizeOfMonth = 31; break;
        case 2: SizeOfMonth = 28; break;
        case 3: SizeOfMonth = 30; break;
        case 4: SizeOfMonth = 31; break;
        case 5: SizeOfMonth = 30; break;
        case 6: SizeOfMonth = 31; break;
        case 7: SizeOfMonth = 31; break;
        case 8: SizeOfMonth = 30; break;
        case 9: SizeOfMonth = 31; break;
        case 10: SizeOfMonth = 30; break;
        case 11: SizeOfMonth = 31; break;
        default:SizeOfMonth = 30; break;
        }
        sleepTime = ((SizeOfMonth - Current.tm_mday) + next.tm_mday - 1) * 24 * 60 * 60 * 1000 + (24 - Current.tm_hour) * 60 * 60 * 1000 + (next.tm_hour * 60 * 60 * 1000 + next.tm_min * 60 * 1000);
    }
    else {
        sleepTime = (next.tm_hour * 60 * 60 * 1000 + next.tm_min * 60 * 1000) - (Current.tm_hour * 60 * 60 * 1000 + Current.tm_min * 60 * 1000);
    }

    return sleepTime - 10 * 60 * 1000;
}

tm currentTime() {
    time_t now;
    now = time(NULL);
    tm Time = *localtime(&now);

    return Time;
}

int main() {
   // signal(SIGINT, mySignalHandler);
    tm Time = currentTime();
    Data newData;
    vector<UniTermin*> Events;

    enum DAY { Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday };
    long Sleeptime = 60000;
    cout << "Uni Scheduler System State" << endl;
    while (Time.tm_mon < 7) {
        Events.clear();
        newData.read();
        Events = newData.getEvents();
        Time = currentTime();

        if (Time.tm_wday == Sunday || Time.tm_wday == Saturday) {
            Sleeptime = (8 * 60 * 60 * 1000)-10*60*1000;
            cout << "Sleep now " << Sleeptime / 1000 / 60 << " minutes" << endl;
            Sleep(Sleeptime);
        }

        else {
            switch (Time.tm_wday) {

            case Monday: {
                for (unsigned int x = 0; x< Events.size(); x++) {
                    if (Events[x]->getTermin().tm_wday == Monday) {
                        cout <<"Events "<< Events[x]->getTermin().tm_hour<<":"<<Events[x]->getTermin().tm_min<<" "<<Events[x]->getTermin().tm_mday<<"."<<Events[x]->getTermin().tm_mon<<"."<<Events[x]->getTermin().tm_year<< "\tCurrent Time:" << Time.tm_hour<<":"<<Time.tm_min<<" "<<Time.tm_mday<<"."<<Time.tm_mon+1<<"."<<Time.tm_year<<endl;
                        if (Events[x]->getTermin().tm_hour == Time.tm_hour && Events[x]->getTermin().tm_min == Time.tm_min && Events[x]->getTermin().tm_mday == Time.tm_mday && Events[x]->getTermin().tm_mon-1 == Time.tm_mon) {
                            shellexec(Events[x]->getUrl());
                            cout << "Stream of " << Events[x]->getName() << " run now" << endl;
                            Events[x]->setTermin(newEventTime(Events[x]->getTermin(),Events[x]->getTurnus()));
                            Sleeptime = sleepCalc(Events[x]->getTermin(), Events[x + 1]->getTermin());
                        }
                    }
                }
                break;
            }
            case Tuesday: {
                for (unsigned int x = 0; x < Events.size(); x++) {
                    if (Events[x]->getTermin().tm_wday == Tuesday) {
                        cout << "Events " << Events[x]->getTermin().tm_hour << ":" << Events[x]->getTermin().tm_min << " " << Events[x]->getTermin().tm_mday << "." << Events[x]->getTermin().tm_mon << "." << Events[x]->getTermin().tm_year << "\tCurrent Time:" << Time.tm_hour << ":" << Time.tm_min << " " << Time.tm_mday << "." << Time.tm_mon + 1 << "." << Time.tm_year << endl;
                        if (Events[x]->getTermin().tm_hour == Time.tm_hour && Events[x]->getTermin().tm_min == Time.tm_min && Events[x]->getTermin().tm_mday == Time.tm_mday && Events[x]->getTermin().tm_mon - 1 == Time.tm_mon) {
                            shellexec(Events[x]->getUrl());
                           cout << "Stream of " << Events[x]->getName() << " run now" << endl;
                            Events[x]->setTermin(newEventTime(Events[x]->getTermin(), Events[x]->getTurnus()));
                            Sleeptime = sleepCalc(Events[x]->getTermin(), Events[x + 1]->getTermin());
                        }
                    }
                }
                break;
            }
            case Wednesday: {
                for (unsigned int x = 0; x< Events.size(); x++) {
                    if (Events[x]->getTermin().tm_wday == Wednesday) {
                        cout << "Events " << Events[x]->getTermin().tm_hour << ":" << Events[x]->getTermin().tm_min << " " << Events[x]->getTermin().tm_mday << "." << Events[x]->getTermin().tm_mon << "." << Events[x]->getTermin().tm_year << "\tCurrent Time:" << Time.tm_hour << ":" << Time.tm_min << " " << Time.tm_mday << "." << Time.tm_mon + 1 << "." << Time.tm_year << endl;
                        if (Events[x]->getTermin().tm_hour == Time.tm_hour && Events[x]->getTermin().tm_min == Time.tm_min && Events[x]->getTermin().tm_mday == Time.tm_mday && Events[x]->getTermin().tm_mon - 1 == Time.tm_mon) {
                            shellexec(Events[x]->getUrl());
                            cout << "Stream of " << Events[x]->getName() << " run now" << endl;
                            Events[x]->setTermin(newEventTime(Events[x]->getTermin(), Events[x]->getTurnus()));
                            Sleeptime = sleepCalc(Events[x]->getTermin(), Events[x + 1]->getTermin());
                        }
                    }
                }
                break;
            }
            case Thursday: {
                for (unsigned int x = 0;x< Events.size(); x++) {
                    if (Events[x]->getTermin().tm_wday == Thursday) {
                        cout << "Events " << Events[x]->getTermin().tm_hour << ":" << Events[x]->getTermin().tm_min << " " << Events[x]->getTermin().tm_mday << "." << Events[x]->getTermin().tm_mon << "." << Events[x]->getTermin().tm_year << "\tCurrent Time:" << Time.tm_hour << ":" << Time.tm_min << " " << Time.tm_mday << "." << Time.tm_mon + 1 << "." << Time.tm_year << endl;
                        if (Events[x]->getTermin().tm_hour == Time.tm_hour && Events[x]->getTermin().tm_min == Time.tm_min && Events[x]->getTermin().tm_mday == Time.tm_mday && Events[x]->getTermin().tm_mon - 1 == Time.tm_mon) {
                            shellexec(Events[x]->getUrl());
                            cout << "Stream of " << Events[x]->getName() << " run now" << endl;
                            Events[x]->setTermin(newEventTime(Events[x]->getTermin(), Events[x]->getTurnus()));
                            Sleeptime = sleepCalc(Events[x]->getTermin(), Events[x + 1]->getTermin());
                        }
                    }
                }
                break;
            }
            case Friday: {
                for (unsigned int x = 0;x< Events.size(); x++) {
                    if (Events[x]->getTermin().tm_wday == Friday) {
                        cout << "Events " << Events[x]->getTermin().tm_hour << ":" << Events[x]->getTermin().tm_min << " " << Events[x]->getTermin().tm_mday << "." << Events[x]->getTermin().tm_mon << "." << Events[x]->getTermin().tm_year << "\tCurrent Time:" << Time.tm_hour << ":" << Time.tm_min << " " << Time.tm_mday << "." << Time.tm_mon + 1 << "." << Time.tm_year << endl;
                        if (Events[x]->getTermin().tm_hour == Time.tm_hour && Events[x]->getTermin().tm_min == Time.tm_min && Events[x]->getTermin().tm_mday == Time.tm_mday && Events[x]->getTermin().tm_mon - 1 == Time.tm_mon) {
                            shellexec(Events[x]->getUrl());
                            cout << "Stream of " << Events[x]->getName() << " run now" << endl;
                            Events[x]->setTermin(newEventTime(Events[x]->getTermin(), Events[x]->getTurnus()));
                            Sleeptime = sleepCalc(Events[x]->getTermin(), Events[x + 1]->getTermin());
                            if (x + 1 == Events.size()) {
                                Sleeptime = sleepCalc(Events[x]->getTermin(), Events[0]->getTermin());
                            }
                            else {
                                Sleeptime = sleepCalc(Events[x]->getTermin(), Events[x + 1]->getTermin());
                            }
                        
                        }
                    }
                }
                break;
            }
            default: {
                cerr << "Matrix error" << endl;
                break;
            }
            }

            if (add == true && addAgain == true) {
                Events.push_back(NewEvent);
                delete NewEvent;
                NewEvent = nullptr;
                add = false; addAgain = false;
            }
            newData.setTmpEvents(Events);
            newData.write();
            Events.clear();
            cout << "Sleep now " << Sleeptime / 1000 / 60 << " minutes" << endl;
            Sleep(Sleeptime);
           Sleeptime = 60000;
        }
    }

    return 0;
}

// Programm ausführen: STRG+F5 oder Menüeintrag "Debuggen" > "Starten ohne Debuggen starten"
// Programm debuggen: F5 oder "Debuggen" > Menü "Debuggen starten"

// Tipps für den Einstieg: 
//   1. Verwenden Sie das Projektmappen-Explorer-Fenster zum Hinzufügen/Verwalten von Dateien.
//   2. Verwenden Sie das Team Explorer-Fenster zum Herstellen einer Verbindung mit der Quellcodeverwaltung.
//   3. Verwenden Sie das Ausgabefenster, um die Buildausgabe und andere Nachrichten anzuzeigen.
//   4. Verwenden Sie das Fenster "Fehlerliste", um Fehler anzuzeigen.
//   5. Wechseln Sie zu "Projekt" > "Neues Element hinzufügen", um neue Codedateien zu erstellen, bzw. zu "Projekt" > "Vorhandenes Element hinzufügen", um dem Projekt vorhandene Codedateien hinzuzufügen.
//   6. Um dieses Projekt später erneut zu öffnen, wechseln Sie zu "Datei" > "Öffnen" > "Projekt", und wählen Sie die SLN-Datei aus.
