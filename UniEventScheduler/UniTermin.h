#pragma once
#include <string>
#include <ctime>

using namespace std;
class UniTermin {
private:
    std::string VorlesungsName;
    int Turnus;
    tm Termin;
    std::string URL;
public:
    UniTermin(string tmpName, int dauer, tm Termin, string url);
    const std::string& getUrl() const;

public:
    const tm& getTermin() const;

public:
    void setTermin(const tm& termin);
    int getTurnus() {
        return this->Turnus;
    }
    string getName() {
        return this->VorlesungsName;
    }



};

