//
// Created by AndyK on 18.04.2020.
//

#include "Data.h"


Data::Data() {
    sourcePath = "Events.txt";
    read();
}

void Data::read() {
    ifstream FILE;
    FILE.open(sourcePath, ios::in);

    if (FILE.fail() == true) {
        cerr << "Termine konnten nicht geladen werden bitte Quelle " << sourcePath << " überprüfen" << endl;
        exit(EXIT_FAILURE);
    }
    else {
        tmpEvents.clear();
        tm tmpTime;
        string Name;
        string Vorlesungsdauer;
        string URL;
        string month, Day, year, hour, min, wday;
        while (FILE.peek() != EOF) {
            string line;
            getline(FILE, line);
            stringstream Buffer(line);
            getline(Buffer, Name, '|');
            getline(Buffer, Vorlesungsdauer, '|');
            getline(Buffer, Day, '|');
            getline(Buffer, month, '|');
            getline(Buffer, year, '|');
            getline(Buffer, hour, '|');
            getline(Buffer, min, '|');
            getline(Buffer, wday, '|');
            getline(Buffer, URL);

            tmpTime.tm_mday = atoi(Day.c_str());
            tmpTime.tm_mon = atoi(month.c_str());
            tmpTime.tm_year = atoi(year.c_str());
            tmpTime.tm_hour = atoi(hour.c_str());
            tmpTime.tm_min = atoi(min.c_str());
            tmpTime.tm_sec = 0;
            tmpTime.tm_wday = atoi(wday.c_str());
            tmpTime.tm_isdst = 0;
            tmpTime.tm_yday = 100;
            tmpEvents.push_back(new UniTermin(Name, atoi(Vorlesungsdauer.c_str()), tmpTime, URL));
        }
    }
    FILE.close();
}

void Data::write() {
    ofstream FILE;
    FILE.open("Events.txt", ios::trunc);

    if (FILE.fail() == true) {
        cerr << "Events konnten nicht gespeichert werden" << endl;
    }
    else {
        for (unsigned int x = 0; x < tmpEvents.size(); x++) {
            FILE << tmpEvents[x]->getName() << '|' << tmpEvents[x]->getTurnus() << '|' << tmpEvents[x]->getTermin().tm_mday << '|' << tmpEvents[x]->getTermin().tm_mon << '|' << tmpEvents[x]->getTermin().tm_year << '|' << tmpEvents[x]->getTermin().tm_hour << '|' << tmpEvents[x]->getTermin().tm_min << '|' << tmpEvents[x]->getTermin().tm_wday << '|' << tmpEvents[x]->getUrl() << "\n";
        }
        FILE.close();
        tmpEvents.clear();
    }
}


