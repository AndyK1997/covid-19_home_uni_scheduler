//
// Created by AndyK on 17.04.2020.
//

#include "UniTermin.h"


UniTermin::UniTermin(string tmpName, int dauer,tm Termin, string url) {
    this->VorlesungsName = tmpName;
    this->Turnus = dauer;
    this->Termin=Termin;
    this->URL = url;
}

const tm& UniTermin::getTermin() const {
    return Termin;
}

void UniTermin::setTermin(const tm& termin) {
    this->Termin = termin;
}

const std::string& UniTermin::getUrl() const {
    return URL;
}

