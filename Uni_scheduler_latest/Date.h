//
// Created by andyk on 18.10.2020.
//

#ifndef UNI_SCHEDULER_DATE_H
#define UNI_SCHEDULER_DATE_H
#include <string>
#include <time.h>
#include <ctime>
class Date {
private:
    time_t timestamp;
    std::string *monthNames;
    std::string *dayNames;
    tm *currentTime;
public:
    Date();
    bool isleapyear(int currentYear);
    std::string getCurrentTime();
    int getMaxOfMonthDays(int month);
    std::string getNameOfMonth(int month);
    std::string getWeekday(int weekDay);
};


#endif //UNI_SCHEDULER_DATE_H
