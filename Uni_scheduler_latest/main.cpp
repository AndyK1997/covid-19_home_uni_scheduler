#include <iostream>
#include "Userinterface.h"
#include "Items/Event.h"
#include "write.h"
#include "Read.h"
#include <thread>
#include <vector>
#include <string>
#include <future>
using namespace std;
vector<Event*> allEvents;
std::string filename="allEvents.txt";
int main() {
   vector<thread> listOfallThreads;
   write writeInstance;
   Read readInstance;
   Userinterface interfaceInstance(allEvents);

   thread readThread(&Read::readfFromFile,&readInstance,ref(allEvents),ref(filename));
   thread interfaceThread(&Userinterface::mainScreen,&interfaceInstance);
   listOfallThreads.push_back(move(interfaceThread));
   listOfallThreads[0].join();
   thread writeThread(&write::writeToFile,&writeInstance,ref(allEvents),ref(filename));
    return 0;
}
