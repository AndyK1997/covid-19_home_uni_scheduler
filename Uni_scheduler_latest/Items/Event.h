//
// Created by AndyK on 15.10.2020.
//

#ifndef UNI_SCHEDULER_EVENT_H
#define UNI_SCHEDULER_EVENT_H

#include <string>

class Event {
private:
    std::string name;
public:
    const std::string &getLink() const;

private:
    std::string link;
public:
    const std::string &getName() const;

private:
    std::string time;
    int rotation;
    int duration;
public:
    Event(std::string name, std::string link, std::string time, int rotation, int duration);

    const std::string &getTime() const;

    int getRotation() const;

    int getDuration() const;

};


#endif //UNI_SCHEDULER_EVENT_H
