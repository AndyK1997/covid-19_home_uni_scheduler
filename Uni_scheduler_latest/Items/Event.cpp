//
// Created by AndyK on 15.10.2020.
//

#include "Event.h"

Event::Event(std::string name, std::string link, std::string time, int rotation, int duration) {
    this->name=name;
    this->link=link;
    this->time=time;
    this->rotation=rotation;
    this->duration=duration;
}

const std::string &Event::getLink() const {
    return link;
}

int Event::getDuration() const {
    return duration;
}

int Event::getRotation() const {
    return rotation;
}

const std::string &Event::getName() const {
    return name;
}

const std::string &Event::getTime() const {
    return time;
}
