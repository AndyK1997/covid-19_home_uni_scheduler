//
// Created by AndyK on 15.10.2020.
//

#ifndef UNI_SCHEDULER_WRITE_H
#define UNI_SCHEDULER_WRITE_H

#include "Items/Event.h"
#include <vector>
#include <fstream>
#include <iostream>
class write {

public:
    void writeToFile(std::vector<Event*> allEvents, std::string& filename) ;
};


#endif //UNI_SCHEDULER_WRITE_H
