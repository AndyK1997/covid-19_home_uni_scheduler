//
// Created by AndyK on 15.10.2020.
//

#include "write.h"


void write::writeToFile(std::vector<Event*> allEvents, std::string& filename) {
    std::fstream file;
    file.open(filename, std::ios::out);
    if(file.fail()==true){
        std::cout<<"Datei konnte nicht geöffnet werden "<<std::endl;
    }else{
        for(unsigned int x=0; x<=allEvents.size(); x++){
            file<<allEvents[x]->getName()<<"|"<<allEvents[x]->getLink()<<"|"<<allEvents[x]->getTime()<<"|"<<allEvents[x]->getRotation()<<"|"<<allEvents[x]->getDuration()<<"\n\r";
        }
        file.close();
    }
}