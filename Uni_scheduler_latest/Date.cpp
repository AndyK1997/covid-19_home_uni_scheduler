//
// Created by andyk on 18.10.2020.
//

#include <iostream>
#include "Date.h"

Date::Date(){
    this->monthNames= new std::string[12]{"Januar", "Februar" , "Maerz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"};
    this->dayNames= new std::string[7]{"Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"};
}


std::string Date::getCurrentTime() {
    this->timestamp=time(0);
    this->currentTime=localtime(&this->timestamp);
    std::string year =std::to_string(this->currentTime->tm_year+1900);
    std::string month =std::to_string(this->currentTime->tm_mon+1);
    std::string day =std::to_string(this->currentTime->tm_mday);
    std::string hour =std::to_string(this->currentTime->tm_hour);
    std::string min =std::to_string(this->currentTime->tm_min);
    return year+"."+month+"."+day+":"+hour+":"+min;
}

int Date::getMaxOfMonthDays(int month) {
    int maxDays=0;
    enum MONTHS{January, February, March, April, May, June, July,August, September, October,November, December };
    switch (month){
        case January : maxDays=31;
            break;
        case February:{
            if(isleapyear(this->currentTime->tm_year+1900)==true){
                maxDays=29;
            }else{
                maxDays=28;
            }
        }
            break;
        case March: maxDays=31;
            break;
        case April: maxDays=30;
            break;
        case May: maxDays=31;
            break;
        case June: maxDays=30;
            break;
        case July : maxDays=31;
            break;
        case August: maxDays=31;
            break;
        case September : maxDays=30;
            break;
        case October: maxDays=31;
            break;
        case November: maxDays=30;
            break;
        case December: maxDays=31;
            break;
        default: std::cerr<<"Error Month does not exist"<<std::endl;
    }
    return maxDays;
}


bool Date::isleapyear(int currentYear) {
    if(((currentYear%4==0) && (currentYear%100!=0)) || (currentYear%400==0)){
        return true;
    } else{
        return false;
    }
}